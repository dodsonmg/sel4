#
# Copyright 2019, Arm Ltd
#
# Copyright 2017, Data61
# Commonwealth Scientific and Industrial Research Organisation (CSIRO)
# ABN 41 687 119 230.
#
# This software may be distributed and modified according to the terms of
# the GNU General Public License version 2. Note that NO WARRANTY is provided.
# See "LICENSE_GPLv2.txt" for details.
#
# @TAG(DATA61_GPL)
#

cmake_minimum_required(VERSION 3.7.2)

declare_platform(virt-aarch64 KernelPlatformVirtAarch64 PLAT_VIRT_AARCH64 KernelSel4ArchAarch64)

if(KernelPlatformVirtAarch64)
    declare_seL4_arch(aarch64)
    set(KernelArmCortexA57 ON)
    set(KernelArchArmV8a ON)
    set(KernelArmPASizeBits44 ON) # is this right?
    config_set(KernelARMPlatform ARM_PLAT virt-aarch64)
    list(APPEND KernelDTSList "tools/dts/virt-aarch64.dts")
    list(APPEND KernelDTSList "src/plat/virt-aarch64/overlay.dts")
    declare_default_headers(
        TIMER_FREQUENCY 62500000llu
        MAX_IRQ 256
        TIMER drivers/timer/arm_generic.h
        INTERRUPT_CONTROLLER arch/machine/gic_v2.h
    )
endif()

add_sources(
    DEP "KernelPlatformVirtAarch64"
    CFILES src/arch/arm/machine/gic_v2.c src/arch/arm/machine/l2c_nop.c
)
